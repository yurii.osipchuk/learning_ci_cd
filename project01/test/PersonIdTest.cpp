#include <iostream>
#include <string>
#include <gtest/gtest.h>
#include "PersonId.h"


class PersonIdTests : public ::testing::Test {
public:
protected:
	PersonId personId;
};


TEST_F(PersonIdTests, emptyValue) {
	std::string value = "null";
	ASSERT_EQ(value, personId.getValue());
}

TEST_F(PersonIdTests, checkValue) {
	std::string value = "0123456789";
	personId.setValue(value);
	ASSERT_EQ(value, personId.getValue());
}

TEST_F(PersonIdTests, badInitLength) {
	std::string valueSmall = "012345678";
	std::string valueBig = "01234567890";
	ASSERT_THROW(personId.setValue(valueSmall), std::logic_error);
	ASSERT_THROW(personId.setValue(valueBig), std::logic_error);
}

TEST_F(PersonIdTests, stringNotNumber) {
	std::string value1 = "abcdqwerty";
	std::string value2 = "a123456789";
	std::string value3 = "012345678b";
	std::string value4 = "0123c56789";
	ASSERT_THROW(personId.setValue(value1), std::logic_error);
	ASSERT_THROW(personId.setValue(value2), std::logic_error);
	ASSERT_THROW(personId.setValue(value3), std::logic_error);
	ASSERT_THROW(personId.setValue(value4), std::logic_error);
}

TEST_F(PersonIdTests, checkDate) {
	std::string value = "1234500015";
	int day = 19;
	int month = 10;
	int year = 1933;
	personId.setValue(value);
	ASSERT_EQ(day, personId.getDay());
	ASSERT_EQ(month, personId.getMonth());
	ASSERT_EQ(year, personId.getYear());
}

TEST_F(PersonIdTests, checkGender) {
	std::string value1 = "1234500000";
	std::string value2 = "1234500010";
	int male = 1;
	int female = 0;
	personId.setValue(value1);
	ASSERT_EQ(female, personId.getGender());
	personId.setValue(value2);
	ASSERT_EQ(male, personId.getGender());
}


TEST_F(PersonIdTests, checkControlSum) {
	std::string value = "1234500015";
	personId.setValue(value);
	ASSERT_TRUE(personId.getControlSum());
}

