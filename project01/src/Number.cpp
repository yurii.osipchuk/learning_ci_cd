#include "Number.h"

Number::Number() : _str("null") {
	_countElements = _str.size();
	fillElements(_elements, _str);
}

Number::~Number() {
}

std::string Number::getStr() {
	return _str;
}

void Number::setStr(std::string &str) {
	_str = str;
	_countElements = _str.size();
	if (_countElements > _lengthMax) {
		throw std::logic_error("Class Number: String length incorrect.");
	}
	fillElements(_elements, _str);
}

void Number::fillElements(std::vector<char> &ar, std::string &str) {
	if(!ar.empty()) {
		ar.clear();
	}

	for (int i = 0; i < str.size(); ++i) {
		ar.push_back(str[i]);
	}
}

int Number::getSize() {
	return _countElements;
}

int Number::getElement(int elementPos) {
	if (elementPos < 0) {
		throw std::logic_error("Class Number: Element position can not be less than zero.");
	}

	if (elementPos > _lengthMax) {
		throw std::logic_error("Class Number: Very large value.");
	}

	// '0' = 48
	return (_elements[elementPos] - 48);
}

bool Number::isNumber() {
	bool result = true;
	for (int i = 0; i < _elements.size(); ++i) {
		if (_elements.at(i) < '0' || _elements.at(i) > '9' ) {
			result = false;
		}
	}
	return result;
}

int Number::pow(int a, int b) {
	int temp = 1;
	if (b == 0) {
		return temp;
	}

	if (b == 1) {
		return a;
	}

	for (int i = 0; i < b; ++i)
	{
		temp *= a;
	}
	return temp;
}
