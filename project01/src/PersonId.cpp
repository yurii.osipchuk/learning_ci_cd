#include "PersonId.h"

PersonId::PersonId() {}

PersonId::~PersonId() {}

std::string PersonId::getValue() {
	return getStr();
}

void PersonId::setValue(std::string &str) {
	setStr(str);
	isComplete = false;

	if (getSize() != _lengthInn) {
		throw std::logic_error("Class PersonId: String length incorrect.");
	}

	if (!isNumber()) {
		throw std::logic_error("Class PersonId: String is not number.");
	}

	Date::setDays(getAllDays());
	isComplete = true;
}

int PersonId::getDay() {
	if (isComplete) {
		return Date::getDay();
	} else {
		return 0;
	}
}

int PersonId::getMonth() {
	if (isComplete) {
		return Date::getMonth();
	} else {
		return 0;
	}
}

int PersonId::getYear() {
	if (isComplete) {
		return Date::getYear();
	} else {
		return 0;
	}
}

int PersonId::getGender() {
	if (_lengthInn != getSize()) {
		return 0;
	}
	int element = getElement(_genderPos - 1);
	return (element % 2 ? 1 : 0);
}

bool PersonId::getControlSum() {
	int control = 0;

	if (_lengthInn != getSize()) {
		return false;
	}

	control += getElement(0) * (-1);
	control += getElement(1) * 5;
	control += getElement(2) * 7;
	control += getElement(3) * 9;
	control += getElement(4) * 4;
	control += getElement(5) * 6;
	control += getElement(6) * 10;
	control += getElement(7) * 5;
	control += getElement(8) * 7;

	control = (control - (11 * (control / 11))) % 10;

	if (control == getElement(_controlPos - 1)) {
		return true;
	} else {
		return false;
	}
}

int PersonId::getAllDays() {
	if (isNumber()) {
		for (int i = 0, j = _datePos - 1; i < _datePos; ++i, --j)
		{
			_daysFromDate += getElement(j) * pow(10, i);
		}
	} else {
		_daysFromDate = 0;
	}
	return _daysFromDate;
}


