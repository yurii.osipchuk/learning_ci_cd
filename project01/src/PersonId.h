#ifndef PERSONID_H_
#define PERSONID_H_

#include "Number.h"
#include "Date.h"
#include <string>

class PersonId : protected Number, protected Date {
public:
	PersonId();
	virtual ~PersonId();

	std::string getValue();
	void setValue(std::string &str);
	int getDay();
	int getMonth();
	int getYear();
	int getGender();
	bool getControlSum();
	int getAllDays();
private:
	int _lengthInn = 10;
	int _genderPos = 9;
	int _datePos = 5;
	int _controlPos = 10;
	bool isComplete = false;
	int _daysFromDate = 0;
};

#endif // PERSONID_H_
