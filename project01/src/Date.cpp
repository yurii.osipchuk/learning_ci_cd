#include "Date.h"


Date::Date() : _days(0), _day(0), _month(0), _year(0) {}
Date::~Date() {}

void Date::setDays(int days) {
	_days = days;
	calculateDate();
}

int Date::getDays() {
	return _days;
}

int Date::getDay() {
	return _day;
}

int Date::getMonth() {
	return _month;
}

int Date::getYear() {
	return _year;
}

int Date::shiftEpoch (int days) {
	//From 1900-01-01 -> 0000-03-01
	const int EpochShiftDays = 693900;
  return days + EpochShiftDays;  
}

int Date::getEraFromDays (int days) {
  const int dEra = 365 * 400 + 97;
  return (days >= 0 ? days : days - (dEra - 1)) / dEra;
}

int Date::getDayOfEra (int days, int era) {
  const int dEra = 365 * 400 + 97;
  return days - era * dEra;
}

int Date::getYearOfEra (int dayOfEra) {
  const int d4Years = 365 * 4 + 1;
  const int d100Years = 365 * 100 + 24;
  const int d400Years = 365 * 400 + 97;
  
  int adjustedDays = dayOfEra - dayOfEra / (d4Years - 1)
                  + dayOfEra / d100Years
                  - dayOfEra / (d400Years - 1);
  return adjustedDays / 365;
}

int Date::getYDay (int dayOfEra, int yearOfEra) {
  return dayOfEra - (365 * yearOfEra + yearOfEra / 4 - yearOfEra / 100);
}

void Date::calculateDate() {
	int days = shiftEpoch(_days);
  int era = getEraFromDays(days);
  int dayOfEra = getDayOfEra(days, era);
  int yearOfEra = getYearOfEra(dayOfEra);
  int shiftedYear = yearOfEra + era * 400;
  int yday = getYDay(dayOfEra, yearOfEra);
  
  //dMarch + dApril + dMay + dJune + dJuly
  int dToAugust = 31 + 30 + 31 + 30 + 31;
  int shiftedMonth = (5 * yday + 2) / dToAugust;
  
  _day = yday - (dToAugust * shiftedMonth + 2) / 5 + 1;
  _month = shiftedMonth + (shiftedMonth < 10 ? 3 : -9);
  _year = shiftedYear + (_month <= 2 ? 1 : 0);
}

void Date::setDate(int day, int month, int year) {
  _day = day;
  _month = month;
  _year = year;
  dateToDays(_day, _month, _year);
}

void Date::dateToDays(int day, int month, int year) {
  if (month <= 2) {
    year -= 1;
  }
  int era = (year >= 0 ? year : year - 399) / 400;
  unsigned yoe = static_cast<unsigned>(year - era * 400);
  unsigned doy = (153*(month + (month > 2 ? -3 : 9)) + 2)/5 + day - 1;
  unsigned doe = yoe * 365 + yoe/4 - yoe/100 + doy;
  _days = era * 146097 + static_cast<int>(doe) - 693900;
}
