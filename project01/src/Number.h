#ifndef NUMBER_H_
#define NUMBER_H_

#include <iostream>
#include <string>
#include <vector>

class Number {
public:
	Number();
	virtual ~Number();

	std::string getStr();
	void setStr(std::string &str);
	int getSize();
	int getElement(int elementPos);
	bool isNumber();
	int pow(int a, int b);
	
private:
	int _lengthMax = 1024;
	int _countElements;
	std::string _str;
	std::vector<char> _elements;

	void fillElements(std::vector<char> &ar, std::string &str);
};

#endif // NUMBER_H_
