#ifndef DATE_H_
#define DATE_H_

// How many days from 01-01-1900 to <set date>

class Date {
public:
	Date();
	virtual ~Date();
	void setDays(int days);
	void setDate(int day, int month, int year);
	int getDays();
	int getDay();
	int getMonth();
	int getYear();
private:
	int _days;
	int _day;
	int _month;
	int _year;

	int shiftEpoch (int days);
	int getEraFromDays (int days);
	int getDayOfEra (int days, int era);
	int getYearOfEra (int dayOfEra);
	int getYDay (int dayOfEra, int yearOfEra);
	void calculateDate();
	void dateToDays(int day, int month, int year);
};

#endif // DATE_H_
